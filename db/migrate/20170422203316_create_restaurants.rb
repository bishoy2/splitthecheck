class CreateRestaurants < ActiveRecord::Migration[5.0]
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :location
      t.integer :numberofupvotes
      t.integer :numberofdownvotes

      t.timestamps
    end
  end
end
