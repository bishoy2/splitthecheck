Rails.application.routes.draw do
  resources :restaurants
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	
	#get 'restaurants/upvote' => 'restaurants#upvote'
	#get 'restaurants/downvote' => 'restaurants#downvote'
	
	
	
	controller :restaurants do
		put :upvote
		put :downvote
	end

end
