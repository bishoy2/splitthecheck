json.extract! restaurant, :id, :name, :location, :numberofupvotes, :numberofdownvotes, :created_at, :updated_at
json.url restaurant_url(restaurant, format: :json)
