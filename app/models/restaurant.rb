class Restaurant < ApplicationRecord
	def incrementupvotes
		@numberofupvotes += 1
	end
	
	def incrementdownvotes
		@numberofdownvotes += 1
	end

	def self.search(search)
  		where("name LIKE ? OR location LIKE ?", "%#{search}%", "%#{search}%") 
	end
end
