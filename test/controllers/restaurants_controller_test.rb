require 'test_helper'

class RestaurantsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @restaurant = restaurants(:one)
  end

  test "should get index" do
    get restaurants_url
    assert_response :success
  end

  test "should get new" do
    get new_restaurant_url
    assert_response :success
  end

  test "should create restaurant" do
    assert_difference('Restaurant.count') do
      post restaurants_url, params: { restaurant: { location: @restaurant.location, name: @restaurant.name, numberofdownvotes: @restaurant.numberofdownvotes, numberofupvotes: @restaurant.numberofupvotes } }
    end

    assert_redirected_to restaurant_url(Restaurant.last)
  end

  test "should show restaurant" do
    get restaurant_url(@restaurant)
    assert_response :success
  end

  test "should get edit" do
    get edit_restaurant_url(@restaurant)
    assert_response :success
  end

  test "should update restaurant" do
    patch restaurant_url(@restaurant), params: { restaurant: { location: @restaurant.location, name: @restaurant.name, numberofdownvotes: @restaurant.numberofdownvotes, numberofupvotes: @restaurant.numberofupvotes } }
    assert_redirected_to restaurant_url(@restaurant)
  end

  test "should destroy restaurant" do
    assert_difference('Restaurant.count', -1) do
      delete restaurant_url(@restaurant)
    end

    assert_redirected_to restaurants_url
  end

	test "should upvote restaurant" do
		upvotes = upvote_path(@restaurant)
		assert upvotes = @restaurant.numberofupvotes + 1
	end
	
	test "should downvote restaurant" do
		downvotes = downvote_path(@restaurant)
		assert downvotes = @restaurant.numberofdownvotes + 1
	end

  test "should find restaurant by name" do
    post restaurants_url, params: { restaurant: { location: @restaurant.location, name: "the pub", numberofdownvotes: 		@restaurant.numberofdownvotes, numberofupvotes: @restaurant.numberofupvotes } }
    @restaurants = Restaurant.search("pub")
    assert_equal 1,@restaurants.count(:name)
  end

  test "should find restaurant by location" do
    post restaurants_url, params: { restaurant: { location: "carrollton", name: @restaurant.name, numberofdownvotes: 		@restaurant.numberofdownvotes, numberofupvotes: @restaurant.numberofupvotes } }
    @restaurants = Restaurant.search("carrollton")
    assert_equal 1,@restaurants.count(:name)
  end

  test "should not find restaurant by name" do
    post restaurants_url, params: { restaurant: { location: @restaurant.location, name: "the pub", numberofdownvotes: 		@restaurant.numberofdownvotes, numberofupvotes: @restaurant.numberofupvotes } }
    @restaurants = Restaurant.search("carrollton")
    assert_equal 0,@restaurants.count(:name)
  end

  test "should not find restaurant by location" do
    post restaurants_url, params: { restaurant: { location: "carrollton", name: @restaurant.name, numberofdownvotes: 		@restaurant.numberofdownvotes, numberofupvotes: @restaurant.numberofupvotes } }
    @restaurants = Restaurant.search("pub")
    assert_equal 0,@restaurants.count(:name)
  end

  test "should find restaurant by name and location" do
    post restaurants_url, params: { restaurant: { location: "Carrollton, GA", name: "Plates", numberofdownvotes: 		@restaurant.numberofdownvotes, numberofupvotes: @restaurant.numberofupvotes } }
    @restaurants = Restaurant.search("plates")
    assert_equal 1,@restaurants.count(:name)
    @restaurants = Restaurant.search("carrollton")
    assert_equal 1,@restaurants.count(:name)
  end

  test "should not find restaurant by name and location" do
    post restaurants_url, params: { restaurant: { location: "Carrollton, GA", name: "Plates", numberofdownvotes: 		@restaurant.numberofdownvotes, numberofupvotes: @restaurant.numberofupvotes } }
    @restaurants = Restaurant.search("McDonald's")
    assert_equal 0,@restaurants.count(:name)
    @restaurants = Restaurant.search("Douglasville")
    assert_equal 0,@restaurants.count(:name)
  end


end
